# sig_docs

sig_docs is a Mkdocs documentation website about GIS.

## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the [requirements.txt](requirements.txt).

```sh
python3 -m pip install --upgrade pip
python3 -m venv virt
source ./virt/bin/activate
python3 -m pip install -r requirements.txt
```

## Usage

```python
source ./virt/bin/activate
mkdocs serve
```

And go to the URL : http://localhost:8000/

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[AGPL](https://choosealicense.com/licenses/agpl-3.0/)
