---
title: Flux carto
summary: Liste des flux cartographiques utiles et disponibles
authors:
    - Julien Monticolo
date: 2022-08-02
---

## Documentation QGIS

- [Création et sélection des serveurs WMS/WMTS](https://docs.qgis.org/latest/fr/docs/user_manual/working_with_ogc/ogc_client_support.html#selecting-wms-wmts-servers)

## Flux image (WMS / WMTS)

- Scans IGN :

    ```
    https://wxs.ign.fr/[demander la clé]/geoportail/wmts?SERVICE=WMTS&REQUEST=GetCapabilities
    ```

- Données Environnement :

    ```
    https://wxs.ign.fr/environnement/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetCapabilities
    ```

- Cartes géologiques :

    ```
    http://geoservices.brgm.fr/geologie
    ```

- Photographies aériennes :

    ```
    https://wxs.ign.fr/ortho/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetCapabilities
    ```

- Photographies aériennes historiques :

    ```
    https://wxs.ign.fr/orthohisto/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetCapabilities
    ```

- Cartes État Major / Occupation du sol historique :

    ```
    https://wxs.ign.fr/cartes/geoportail/r/wms?SERVICE=WMS&VERSION=1.3.0&REQUEST=GetCapabilities
    ```

- Occupation du sol Grande Échelle :

    ```
    https://wxs.ign.fr/ocsge/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetCapabilities
    ```

- Parcellaire :

    ```
    https://wxs.ign.fr/parcellaire/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetCapabilities
    ```

- Registre Parcellaire Graphique :

    ```
    https://wxs.ign.fr/agriculture/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetCapabilities
    ```

- Cartes et plans :

    ```
    https://wxs.ign.fr/cartes/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetCapabilities
    ```

- Autres cartes, plans et photographies aériennes historiques :

    ```
    https://ws.sogefi-web.com/wms?
    ```
    > Détail :
    >
    > - Carte de Cassini (XVIIIème siècle)
    > - Carte de Capitaine (XVIIIème siècle)
    > - Carte du Service Vicinal (XIXème siècle)
    > - Photographies aériennes anciennes géoréférencées


- Corine Land Cover :

    ```
    https://wxs.ign.fr/clc/geoportail/wmts?SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetCapabilities
    ```

## Flux vecteur (WFS)

